import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AgmCoreModule } from '@agm/core'

import { AppComponent } from './components/app/app.component'
import { MissionsComponent } from './components/missions/missions.component'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlacesComponent } from './components/google-places/google-places.component'

@NgModule( {
	declarations: [
		AppComponent,
		MissionsComponent,
		GooglePlacesComponent
	],
	imports: [
		AgmCoreModule.forRoot( {
			apiKey: 'AIzaSyCMkSNjOWrI9A7vg2upFLY79WaRvEwCnfo',
			libraries: [ 'places' ],
			region: 'en'
		} ),
		BrowserModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule
	],
	providers: [],
	bootstrap: [ AppComponent ]
} )
export class AppModule {
}
