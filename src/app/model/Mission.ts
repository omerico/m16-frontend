export default class Mission {
	id: number = 0
	agent_name: string
	country: string
	address: string
	datetime: string
}
