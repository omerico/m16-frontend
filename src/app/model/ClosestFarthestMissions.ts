import Mission from './Mission'

export default class ClosestFarthestMissions {
	closest: Mission = new Mission()
	farthest: Mission = new Mission()
}
