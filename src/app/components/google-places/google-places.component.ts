import { Component, ElementRef, EventEmitter, NgZone, OnInit, Output, ViewChild } from '@angular/core'
import { FormControl } from '@angular/forms'
import { MapsAPILoader } from '@agm/core'

declare var google

@Component( {
	selector: 'app-google-places',
	templateUrl: './google-places.component.html'
} )
export class GooglePlacesComponent implements OnInit {
	public searchControl: FormControl = new FormControl()

	@Output()
	public placeChanged: EventEmitter<string> = new EventEmitter<string>()

	@ViewChild( 'search' )
	public searchElementRef: ElementRef

	public constructor( private readonly mapsAPILoader: MapsAPILoader, private readonly ngZone: NgZone ) {
	}

	public ngOnInit() {
		this.mapsAPILoader.load().then( () => {
			const autocomplete = new google.maps.places.Autocomplete( this.searchElementRef.nativeElement, {
				types: [ 'address' ]
			} )

			autocomplete.addListener( 'place_changed', () => {
				this.ngZone.run( () => {
					const place: any = autocomplete.getPlace()
					this.placeChanged.emit( place.formatted_address )
					if ( place.geometry === undefined || place.geometry === null ) {
						return
					}
				} )
			} )
		} )
	}
}
