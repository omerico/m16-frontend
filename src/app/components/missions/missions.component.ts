import { Component, OnInit } from '@angular/core'
import { RestApiService } from '../../services/rest-api.service'
import Mission from '../../model/Mission'
import Country from '../../model/Country'
import ClosestFarthestMissions from '../../model/ClosestFarthestMissions'

@Component( {
	selector: 'app-missions',
	templateUrl: './missions.component.html',
	styleUrls: [ './missions.component.scss' ]
} )
export class MissionsComponent implements OnInit {
	public missions: Mission[] = []
	public isolatedCountries: Country[] = null
	public closestAndFarthestLocations: ClosestFarthestMissions = new ClosestFarthestMissions()

	public constructor( private readonly restApiService: RestApiService ) {
	}

	public ngOnInit(): void {
		this.restApiService.getAllMissions().then( ( response: Mission[] ) => {
			this.missions = response
		} )
	}

	public findIsolatedCountries(): void {
		this.isolatedCountries = []

		this.restApiService.findMostIsolatedCountry().then( ( response: Country[] ) => {
			this.isolatedCountries = response
		} )
	}

	private markClosest( address: string ): void {
		this.closestAndFarthestLocations = new ClosestFarthestMissions()

		this.restApiService.findClosestAndFarthestLocations( address ).then( ( response: ClosestFarthestMissions ) => {
			if ( response !== null ) {
				this.closestAndFarthestLocations = response
			} else {
				alert( `No results found for ${ address }` )
			}
		} )
	}
}
