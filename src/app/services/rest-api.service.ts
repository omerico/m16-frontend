import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import Mission from '../model/Mission'
import Country from '../model/Country'
import ClosestFarthestMissions from '../model/ClosestFarthestMissions'

@Injectable( {
	providedIn: 'root'
} )
export class RestApiService {
	private static readonly ENDPOINT: string = 'http://localhost:3000/missions'

	constructor( private readonly http: HttpClient ) {
	}

	public async getAllMissions(): Promise<Mission[]> {
		return await this.http.get<Mission[]>( `${ RestApiService.ENDPOINT }` ).toPromise()
	}

	public async findMostIsolatedCountry(): Promise<Country[]> {
		return await this.http.get<Country[]>( `${ RestApiService.ENDPOINT }/countries-by-isolation` ).toPromise()
	}

	public async findClosestAndFarthestLocations( address: string ): Promise<ClosestFarthestMissions> {
		return await this.http.post<ClosestFarthestMissions>( `${ RestApiService.ENDPOINT }/find-closest`, { 'target-location': address } )
			.toPromise()
	}
}
